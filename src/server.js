/**
 * Created by Chris on 17/4/26.
 */

import jsonServer from 'json-server';
import bodyParser from 'body-parser';
import {Router} from './route';
import config from 'config';

class Server {

    start() {
        var server = jsonServer.create();
        var db = jsonServer.router(config.get('db.path'));
        var middlewares = jsonServer.defaults();
        var router = new Router(server);

        server.use(middlewares);
        server.use(bodyParser.json());
        router.initRoute();
        server.use(db);
        server.listen(config.get('server.port'), function () {
            console.log('JSON Server is running');
        })
    }
}

export {Server};
