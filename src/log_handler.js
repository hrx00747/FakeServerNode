import fs from 'fs';
import config from 'config';
import datetime from 'node-datetime';

const logConfig = config.get('log');

class LogHandler {

    addLog(req,res) {
        var body = req.body;
        let ymd = datetime.create().format('Ymd');
        var fileName = body.platform+"_"+body.clientId+"_"+body.projectName+"_"+ymd;
        var path = logConfig.get('location')+"/"+fileName;
        var time = datetime.create().format('Y-m-d H:M:S N');
        var data = body.data;
        var tag = body.tag;
        var content = "";
        if (typeof data == 'string' || typeof data == 'number') {
            content = "\n"+time+"\t"+tag+"\t"+data;
        }
        else{
            content = "\n"+time+"\t"+tag+"\n"+JSON.stringify(data,null,4);
        }
        fs.appendFile(path,content,'utf-8', function(error,data){
            if(error){
                console.log(error);
                res.send("failed");
            }
            else{
                console.log('write log finished');
                res.send("success");
            }
        });
    }
}

export {LogHandler};

