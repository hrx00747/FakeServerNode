/**
 * Created by Chris on 17/5/2.
 */

import {JsonHelper} from './json_helper';
import config from 'config'

const apiConfig = config.get('api');

class ApiHandler {

    apiNameList(req, res) {

        JsonHelper.readJson(apiConfig.get('path'), function (error,json) {
            var apiNames = Object.keys(json);

            res.send(apiNames);
        })
    }

    apiList(req, res) {

        JsonHelper.readJson(apiConfig.get('path'), function (error,json) {
            var apis = Object.values(json)

            res.send(apis);
        })

    }

    getApi(req, res) {

        JsonHelper.readJson(apiConfig.get('path'),function (error, json) {
            var path = req.params['path'];
            var obj = json[path];

            res.json(obj)
        })
    }

    postApi(req, res) {

        console.log(req.body);
        var path = apiConfig.get('path');
        JsonHelper.readJson(path, function (error, json) {
            var newJson = req.body
            var result = JsonHelper.mergeJson(json, newJson);
            JsonHelper.writeJson(path, result);

            res.send(newJson);
        })
    }

    deleteApi(req, res) {

        var path = apiConfig.get('path');
        JsonHelper.readJson(path,function (error, json) {
            var path = req.params['path'];
            var obj = json[path];
            json[path] = undefined;
            JsonHelper.writeJson(path,json);

            res.send(obj);
        })
    }
}

export {ApiHandler};