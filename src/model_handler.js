/**
 * Created by Chris on 17/5/2.
 */
import {JsonHelper} from './json_helper';
import {Mocker} from './mocker/mocker'
import config from 'config';

const modelConfig = config.get('model');

class ModelHandler {

    modelNameList(req, res) {

        var path = modelConfig.get("path");
        JsonHelper.readJson(path, function (error,json) {
            var modelNames = Object.values(json);

            res.send(modelNames);
        })
    }

    modelList(req, res) {

        var path = modelConfig.get("path");
        JsonHelper.readJson(path, function (error,json) {
            var models = Object.keys(json);

            res.send(models);
        })

    }

    getModel(req, res) {

        var path = modelConfig.get("path");
        JsonHelper.readJson(path,function (error, json) {
            var path = req.params['name'];
            var obj = json[path];

            res.json(obj)
        })
    }

    postModel(req, res) {

        console.log(req.body);
        var path = modelConfig.get("path");
        JsonHelper.readJson(path, function (error, json) {
            var newJson = req.body
            var result = JsonHelper.mergeJson(json, newJson);
            JsonHelper.writeJson(modelJsonName, result);

            res.send(newJson);
        })
    }

    deleteModel(req, res) {

        var path = modelConfig.get("path");
        JsonHelper.readJson(path,function (error, json) {
            var path = req.params['name'];
            var obj = json[path];
            json[path] = undefined;
            JsonHelper.writeJson(modelJsonName,json);

            res.send(obj);
        })
    }


    generateModelData(req, res) {

        var body = req.body;
        var result = Mocker.getInstance(body).generate();

        res.send(result);
    }
}

export {ModelHandler};