/**
 * Created by Chris on 17/5/4.
 */

class Base {

    constructor(number){
        this.number = number;
    }

    generate() {

        if (this.number && this.number > 0){
            var jsonList = [];
            for(var i=0;i<this.number;i++){
                jsonList.push(this.generateTemplate());
            }
            return jsonList;
        }
        else{
            return this.generateTemplate();
        }
    }

    generateTemplate() {

    }
}

export {Base};