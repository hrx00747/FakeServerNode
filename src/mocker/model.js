/**
 * Created by Chris on 17/5/4.
 */

import {Base} from './base';
import {Mocker} from './mocker';

class Model extends Base {

    /**
     *
     * @param json
     * json string
     */
    constructor(json, number) {
        super(number);
        this.json = json;
    }

    generateTemplate() {
        var templates = this.json;
        var json = {};
        templates.forEach(function (template) {
            json[template.name] = Mocker.getInstance(template).generate();
        })

        return json;
    }

}

export {Model};