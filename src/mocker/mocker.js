/**
 * Created by Chris on 17/5/2.
 */

import {Avatar} from './field/avatar';
import {Image} from './field/image';
import {Title} from './field/title';
import {Name} from  './field/name';
import {Phone} from  './field/phone';
import {Address} from './field/address';
import {BOOL} from './field/bool';
import {DateTime} from './field/date';
import {Timestamp} from './field/timestamp';
import {Integer} from './field/integer';
import {Float} from './field/float';
import {Text} from './field/text';
import {Web} from './field/web';
import {ZipCode} from './field/zipcode';
import {Model} from  './model';
import {JsonHelper} from "../json_helper";


class Mocker {

    static classess() {
        return {
            Avatar,
            Image,
            Title,
            Name,
            Phone,
            Address,
            BOOL,
            DateTime,
            Timestamp,
            Integer,
            Float,
            Text,
            Web,
            ZipCode
        }
    }

    static createFieldInstance(json) {

        var instance;
        if (json.value.length > 0) {
            var arr = json.value.split('|');
            var name = arr[0];
            var className = Mocker.classess()[name];
            if (className){
                var params = [];
                if (arr.length > 1){
                    params = arr[1].split(',');
                }
                if (json.number){
                    params.push(json.number);
                }
                instance = new className(...params);
            }
            else{
                var models = Mocker.models();
                if (models[name]){
                    var subJson = {
                        name: name,
                        value: models[name],
                        number: json.number
                    }
                    instance = Mocker.createModelInstance(subJson);
                }
            }
        }

        return instance;
    }

    static createModelInstance(json) {

        return new Model(json.value, json.number);
    }

    static getInstance(json) {

        var instance;
        if (typeof(json.value) == 'string'){
            instance = Mocker.createFieldInstance(json);
        }
        else if(Array.isArray(json.value)){
            instance = Mocker.createModelInstance(json);
        }
        return instance;
    }

    static models() {
        return JsonHelper.readJsonSyn('model.json');
    }
}

export {Mocker};