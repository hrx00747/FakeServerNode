/**
 * Created by Chris on 17/5/2.
 */

import {Random} from 'mockjs';
import {Base} from '../base';


class Name extends Base {

    /**
     *
     * @param lang
     * lang = cn|other
     */
    constructor(lang, number) {
        super(number);
        this.lang = lang;
    }


    generateTemplate() {
        if (this.lang == 'cn') {
            return Random.cname();
        }
        else {
            return Random.name();
        }
    }
}

export {Name};