/**
 * Created by Chris on 17/5/9.
 */

import {Random} from 'mockjs';
import {Base} from '../base';

class Timestamp extends Base {

    generateTemplate() {
        var today = new Date();
        var timestamp = Math.round(today.getTime());
        var delta = Math.round(Math.random()*100000);
        return timestamp - delta;
    }
}

export {Timestamp};