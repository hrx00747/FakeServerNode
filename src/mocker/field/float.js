/**
 * Created by Chris on 17/5/4.
 */


import {Random} from 'mockjs';
import {Base} from '../base';

class Float extends Base{

    constructor(digit, number){
        super(number);
        this.digit = digit;
    }

    generateTemplate() {

        var number = Random.float(1, 1000000, this.digit, this.digit);

        return number;
    }

}

export {Float};