/**
 * Created by Chris on 17/5/4.
 */


import {Random} from 'mockjs';
import {Base} from '../base';

class ZipCode extends Base {

    generateTemplate() {

        return Random.zip();
    }
}

export {ZipCode};