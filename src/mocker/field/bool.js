/**
 * Created by Chris on 17/5/4.
 */

import {Random} from 'mockjs';
import {Base} from '../base';

class BOOL extends Base{


    generateTemplate() {

        return Random.boolean();
    }
}

export {BOOL};