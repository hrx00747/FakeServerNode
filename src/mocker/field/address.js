/**
 * Created by Chris on 17/5/4.
 */

import {Random} from 'mockjs';
import {Base} from '../base';

class Address extends Base {

    constructor(type,number) {
        super(number);
        this.type = type;
    }

    generateTemplate() {

        var address = "未知地址";
        switch(this.type) {
            case 'region':
                address = Random.region();
                break;
            case 'province':
                address = Random.province();
                break;
            case 'city':
                address = Random.city();
                break;
            case 'county':
                address = Random.county();
                break;
            case 'province_city':
                address = Random.city(true);
                break;
            case 'province_city_country':
                address = Random.county(true);
                break;
            case 'detail':
                address = '详细地址';
                break;
            default:
                address = '未知类型';
        }


        return address.replace("/\s/g","");
    }
}

export {Address};