/**
 * Created by Chris on 17/5/4.
 */

import {Random} from 'mockjs';
import {Base} from '../base';

class Text extends Base{

    constructor(type, min, max, number) {
        super(number);
        this.type = type;
        this.min = min;
        this.max = max;
    }

    generateTemplate() {

        var result;
        switch (this.type){
            case 'paragraph':
                result = Random.cparagraph(this.min, this.max);
                break;
            case 'sentence':
                result = Random.csentence(this.min, this.max);
                break;
            case 'word':
                result = Random.cword(this.min, this.max);
                break;
            default:
                result = Random.text();
        }

        return result;
    }
}

export {Text};