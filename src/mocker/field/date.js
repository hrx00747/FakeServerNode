/**
 * Created by Chris on 17/5/4.
 */

import {Random} from 'mockjs';
import {Base} from '../base';

class DateTime extends Base {

    constructor(type, format, number) {
        super(number);
        this.type = type;
        this.format = format;
    }

    generateTemplate() {

        var result;
        switch (this.type) {
            case 'date':
                result = Random.date(this.format);
                break;
            case 'time':
                result = Random.time(this.format);
                break;
            case 'datetime':
                result = Random.datetime(this.format);
                break;
            default:
                result = Random.now();
        }


        return result;

    }
}

export {DateTime};