/**
 * Created by Chris on 17/5/4.
 */


import {Random} from 'mockjs';
import {Base} from '../base';

class Integer extends Base{

    constructor(min, max, number) {
        super(number);
        this.min = min;
        this.max = max;
    }

    generateTemplate() {

        var number = Random.integer(this.min, this.max);
        return number;
    }
}

export {Integer};