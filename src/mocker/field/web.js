/**
 * Created by Chris on 17/5/4.
 */

import {Random} from 'mockjs';
import {Base} from '../base';

class Web extends Base {

    constructor(type, number){
        super(number);
        this.type = type;
    }

    generateTemplate() {

        var result;
        switch (this.type){
            case 'url':
                result = Random.url();
                break;
            case 'domain':
                result = Random.domain();
                break;
            case 'email':
                result = Random.email();
                break;
            case 'ip':
                result = Random.ip();
                break;
            default:
                result = '未知Web元素';
        }

        return result;
    }
}

export {Web};