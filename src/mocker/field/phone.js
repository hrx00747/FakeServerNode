/**
 * Created by Chris on 17/5/2.
 */

import {Random} from 'mockjs';
import {JsonHelper} from '../../json_helper';
import {Base} from '../base';

var areaCodeName = 'area_code.json';
var defaultAreaCode = "010";

class Phone extends Base {


    /**
     *
     * @param type
     * type = mobile | other
     */
    constructor(type, number) {
        super(number);
        this.type = type;
    }

    generateTemplate() {

        if (this.type == 'mobile') {
            return Random.natural(13400000000,13900000000);
        }
        else{
            var json = JsonHelper.readJsonSyn(areaCodeName);
            var code = defaultAreaCode;
            if (json.length > 0) {
                var index = Math.round(json.length * Math.random());
                code = json[index];
            }

            var number = Random.natural(-5000000,5000000);

            return code+'-'+number;
        }
    }

}

export {Phone};