/**
 * Created by Chris on 17/5/4.
 */


import {Random} from 'mockjs';
import {Base} from '../base';

class Title extends Base{


    generateTemplate() {

        return Random.text();
    }
}

export {Title};