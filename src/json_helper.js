/**
 * Created by Chris on 17/5/2.
 */

import fs from 'fs';

class JsonHelper {

    static readJson(name, callback) {

        fs.readFile(name, 'utf-8', function (error, data) {

            var json = {};
            if (data != undefined) {
                json = JSON.parse(data);
                console.log('json = '+json);
            }
            callback(error, json);
        })
    }

    static readJsonSyn(name) {
        var data = fs.readFileSync(name, 'utf-8');
        var json = {};
        if (data != undefined) {
            json = JSON.parse(data);
        }

        return json;
    }


    static writeJson(name, json) {

        var jsonText = JSON.stringify(json,null, '\t');
        fs.writeFile(name,jsonText,'utf-8',function (error, data) {
            if (error) {
                console.log(error);
            }
            else {
                console.log('write json finished');
            }

        })
    }


    static mergeJson(source, target) {
        var json = source;
        for (var key in target) {
            json[key] = target[key];
        }

        return json;
    }
}

export {JsonHelper};