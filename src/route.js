/**
 * Created by Chris on 17/4/28.
 */


import {ApiHandler} from './api_handler';
import {ModelHandler} from './model_handler';
import {RouteHandler} from './route_handler';
import {LogHandler} from './log_handler';
import bodyParser from 'body-parser';

class Router {

    constructor(server) {
        this.server = server;
    }

    initRoute() {

        var apiHandler = new ApiHandler();
        var modelHandler = new ModelHandler();
        var routeHandler = new RouteHandler();
        var logHandler = new LogHandler();

        //api
        this.server.get("/admin/apiNames", apiHandler.apiNameList);
        this.server.get("/admin/apis", apiHandler.apiList);
        this.server.get("/admin/apis/:path", apiHandler.getApi);
        this.server.post("/admin/apis", bodyParser.json(), apiHandler.postApi);
        this.server.delete("/admin/apis/:path", apiHandler.deleteApi);

        //model
        this.server.get("/admin/modelNames",modelHandler.modelNameList);
        this.server.get("/admin/models", modelHandler.modelList);
        this.server.get("/admin/models/:name", modelHandler.getModel);
        this.server.post("/admin/models", bodyParser.json(), modelHandler.postModel);
        this.server.delete("/admin/models/:name", modelHandler.deleteModel);

        //log
        this.server.post("/admin/logs",logHandler.addLog);

        //generate data
        this.server.post("/generate", bodyParser.json(), modelHandler.generateModelData);

        //custom
        this.server.get("/*", routeHandler.get);
        this.server.post("/*", bodyParser.json(), routeHandler.post);
        this.server.put("/*", bodyParser.json(), routeHandler.put);
        this.server.delete("/*", bodyParser.json(), routeHandler.delete);
    }
}

export {Router};
