/**
 * Created by Chris on 2017/7/6.
 */

import {JsonHelper} from './json_helper'
import {Url} from 'url'
import config from 'config';

const apiConfig = config.get('api');

class RouteHandler {

    get(req, res) {

        var path = apiConfig.get("path");
        let url = (new Url()).parse(req.url)
        JsonHelper.readJson(path, function (error,json) {
            let apis = Object.values(json)
            var result = {}
            apis.forEach(function (api) {
                if (api.method == 'GET' && api.path == url.pathname){
                    result = api
                }
            })
            res.send(result.response)
        })
    }

    post(req, res) {

        var path = apiConfig.get("path");
        let url = (new Url()).parse(req.url)
        JsonHelper.readJson(path, function (error,json) {
            let apis = Object.values(json)
            var result = {}
            apis.forEach(function (api) {
                if (api.method == 'POST' && api.path == url.pathname){
                    result = api
                }
            })
            res.send(result.response)
        })
    }

    put(req, res) {

        var path = apiConfig.get("path");
        let url = (new Url()).parse(req.url)
        JsonHelper.readJson(path, function (error,json) {
            let apis = Object.values(json)
            var result = {}
            apis.forEach(function (api) {
                if (api.method == 'PUT' && api.path == url.pathname){
                    result = api
                }
            })
            res.send(result.response)
        })
    }

    delete(req, res){

        var path = apiConfig.get("path");
        let url = (new Url()).parse(req.url)
        JsonHelper.readJson(path, function (error,json) {
            let apis = Object.values(json)
            var result = {}
            apis.forEach(function (api) {
                if (api.method == 'DELETE' && api.path == url.pathname){
                    result = api
                }
            })
            res.send(result.response)
        })
    }
}

export {RouteHandler};